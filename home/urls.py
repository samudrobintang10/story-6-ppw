from django.urls import path
from django.conf.urls import url 

from . import views

app_name = 'home'

urlpatterns = [
    path('', views.index, name='index'),
    path('newpage/', views.newpage, name='newpage'),
    path('jadwal/', views.jadwal, name='jadwal'),
    path('peserta/<int:id_peserta>', views.peserta, name='peserta'),
    path('delete/<int:delete_id>', views.delete, name='delete'),
]