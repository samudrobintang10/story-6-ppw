from django.shortcuts import render, redirect

from .forms import PostForm, PostPeserta
from .models import Kegiatan, Peserta

nama_pribadi = "Bintang Samudro"

# Create your views here.

def delete(request, delete_id):
    Kegiatan.objects.filter(id=delete_id).delete()
    return redirect('home:jadwal')


def index(request):
    response = {'name' : nama_pribadi}
    return render(request, 'home/index.html', response)

def newpage(request):
    post_form = PostForm(request.POST or None)
    # error = None

    if request.method == 'POST':

        if post_form.is_valid():
            post_form.save()

            return redirect('home:jadwal')

    context = {
        'page_title':'Susun Kegiatan',
        'post_form':post_form,
    }
    return render(request, 'home/newpage.html', context)

def jadwal(request):
    posts = Kegiatan.objects.all()
    peserta = Peserta.objects.all()
    context = {
        'page_title':'List Kegiatan',
        'posts':posts,
        'peserta':peserta,
    }

    return render(request, 'home/jadwal.html', context)

def peserta(request, id_peserta):
    post_peserta = PostPeserta(request.POST or None)
    # error = None

    if request.method == 'POST':

        if post_peserta.is_valid():
            peserta = Peserta(ikut_kegiatan=Kegiatan.objects.get(id=id_peserta), nama_peserta=post_peserta.data['nama_peserta'])
            peserta.save()

            return redirect('home:jadwal')
    else:
        post_peserta = PostPeserta()

    context = {
        'page_title':'Tambah Peserta',
        'post_peserta':post_peserta,
    }
    return render(request, 'home/peserta.html', context)
    