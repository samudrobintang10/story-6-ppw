from django import forms


# import model dari models.py
from .models import Kegiatan, Peserta

class PostForm(forms.ModelForm):
    class Meta:
        model = Kegiatan
        fields = [
            'nama_kegiatan',
        ]

        labels = {
            'nama_kegiatan':'Nama Kegiatan',
        }

        widgets = {
            'nama_kegiatan': forms.TextInput(
                attrs= {
                    'class':'form-control',
                    'placeholder':'Contoh: Rapat Dengar Pendapat',
                }
            ),
        }

class PostPeserta(forms.ModelForm):
    class Meta:
        model = Peserta
        fields = [
            'nama_peserta',
        ]

        labels = {
            'nama_peserta':'Nama Peserta',
        }

        widgets = {
            'nama_peserta': forms.TextInput(
                attrs= {
                    'class':'form-control',
                    'placeholder':'Contoh: Cristiano Ronaldo',
                }
            ),
        }
