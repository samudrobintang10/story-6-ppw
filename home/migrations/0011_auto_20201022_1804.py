# Generated by Django 3.1.1 on 2020-10-22 11:04

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('home', '0010_auto_20201022_1657'),
    ]

    operations = [
        migrations.RenameModel(
            old_name='Lesson',
            new_name='Kegiatan',
        ),
    ]
