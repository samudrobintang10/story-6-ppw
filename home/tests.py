from django.test import TestCase, Client
from django.urls import resolve
# Create your tests here.

from .views import index, newpage, jadwal
from .models import Kegiatan, Peserta

class TestKegiatan(TestCase):
    def test_jadwal_url_kegiatan_is_exist(self):
        response = Client().get('/jadwal/')
        self.assertEqual(response.status_code, 200)

    def test_jadwal_index_func(self):
        found = resolve("/jadwal/")
        self.assertEqual(found.func, jadwal)

    def test_jadwal_using_template(self):
        response = Client().get('/jadwal/')
        self.assertTemplateUsed(response, 'home/jadwal.html')

class TestTambahKegiatan(TestCase):
    def test_tambah_kegiatan_url_is_exist(self):
        response = Client().get('/newpage/')
        self.assertEqual(response.status_code, 200)

    def test_tambah_kegiatan_index_func(self):
        found = resolve('/newpage/')
        self.assertEqual(found.func, newpage)

    def test_tambah_kegiatan_using_template(self):
        response = Client().get("/newpage/")
        self.assertTemplateUsed(response, 'home/newpage.html')

    def test_kegiatan_model_create_new_object(self):
        kegiatan = Kegiatan(nama_kegiatan="Main Futsal")
        kegiatan.save()
        self.assertEqual(Kegiatan.objects.all().count(), 1)
    
    def test_kegiatan_url_post_is_exist(self):
        response = Client().post('/jadwal/', data={'nama_kegiatan':'Makan Bareng'})
        self.assertEqual(response.status_code, 200)

class TestRegist(TestCase):
    def setUp(self):
        kegiatan = Kegiatan(nama_kegiatan='Masak Bersama')
        kegiatan.save()

    def test_regist_url_post_is_exist(self):
        response = Client().post('/peserta/1', data={'nama_peserta':'Ronaldo'})
        self.assertEqual(response.status_code, 302)

    def test_regist_url_is_exist(self):
        response = Client().get('/peserta/1')
        self.assertEqual(response.status_code, 200)

class TestHapusKegiatan(TestCase):
    def setUp(self):
        kegiatan = Kegiatan(nama_kegiatan='Futsal')
        kegiatan.save()
    
    def test_hapus_url_post_is_exist(self):
        response = Client().post('/delete/1')
        self.assertEqual(response.status_code, 302)
    
    def test_hapus_url_is_exist(self):
        response = Client().get('/delete/1')
        self.assertEqual(response.status_code, 302)